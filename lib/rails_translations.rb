require "rails_translations/version"
require "custom_hash"

module RailsTranslations
  class Error < StandardError; end

  using CustomHash

  class << self

    def equilize_translations(options={})
      file_names = options[:file_names] ? options[:file_names] : get_translation_file_names
      file_names.each do |file_name|
        equilize_files(file_name, options)
      end
    end

    def equilize_files(file_name, options={})
      equilize_locales = options[:equilize_locales] ? options[:equilize_locales] : I18n.available_locales
      take_from_locales = options[:take_from_locales] ? options[:take_from_locales] : I18n.available_locales
      prefix = options[:prefix].blank? ? nil : options[:prefix]

      equilize_locales.each do |locale|
        keys = load_keys(file_name, locale)
        other_locales = take_from_locales - [ locale ]
        other_locales.each do |other_locale|
          other_keys = load_keys(file_name, other_locale).prefix_deep_values_with()
          keys = other_keys.deep_merge(keys)
        end
        write_to_yml_file(file_name, locale, keys)
      end
    end

    def build_attributes_placeholders(options={})
      models_attributes = collect_attributes_from_models
      write_to_locales = options[:write_to_locales] ? options[:write_to_locales] : I18n.available_locales

      file_name = options[:file_name].blank? ? 'models_fields' : options[:file_name]

      write_to_locales.each do |locale|
        write_to_yml_file(
          file_name,
          locale,
          models_attributes.deep_merge(load_keys(file_name, locale))
        )
      end
    end

    def get_translations_files_list
      get_translations_files_paths.map{ |f| f.split(File::SEPARATOR).last }
    end

    private

    def collect_attributes_from_models(options={})
      models_attributes = { activerecord: { attributes: {} }}
      application_models.each do |model|
        attributes = model.new.attributes.keys - ["created_at", "updated_at", "id"]
        if attributes.any?
          model_attributes = {}
          (attributes).each do |key|
            model_attributes.merge!({ :"#{key}" => '' })
          end
          models_attributes[:activerecord][:attributes][model.name.underscore] = model_attributes
        end
      end
      models_attributes.deep_stringify_keys
    end

    def application_models
      Rails.application.eager_load!
      (ActiveRecord::Base.descendants - [ApplicationRecord])
    end

    def load_translation_file(file_name)
      begin
        YAML::load(File.read(File.join(Rails.root, 'config', 'locales', file_name)))
      rescue Errno::ENOENT
        {}
      end
    end

    def get_translations_files_paths
      Dir.glob(File.join("config", "locales", "*"))
    end

    def get_translation_file_names
      get_translations_files_list.map{ |f| f.gsub(f.split(".")[-2..-1].join("."), "").sub(/\.$/, "") }.flatten.uniq
    end

    def load_keys(file_name, locale)
      load_translation_file([file_name, locale, 'yml'].reject(&:blank?).join("."))[locale.to_s].to_h
    end

    def write_to_yml_file(file_name, locale, keys)
      file = File.join(Rails.root, 'config', 'locales', [ file_name, locale, "yml" ].reject(&:blank?).join("."))
      file_keys = { :"#{locale}" => keys }
      open(file, 'w') do |f|
        f.puts file_keys.to_yml
      end
    end

  end
end
