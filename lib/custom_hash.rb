module CustomHash
	refine Hash do

		def set_deep_values(new_value)
			self.keys.each do |key|
				self[key].is_a?(Hash) ? self[key].set_deep_values(new_value) : self[key] = new_value
			end
			self
		end

		def prefix_deep_values_with(prefix="TODO:")
			self.keys.each do |key|
				self[key].is_a?(Hash) ? self[key].prefix_deep_values_with(prefix) : self[key] = "#{prefix} #{self[key]}"
			end
			self
		end

		def to_yml(identation=0)
			yml = ""
			self.each do |key, value|
				key = "\"#{key}\"" if key.in? yml_reserved_words
				if value.is_a?(Hash)
					yml += "#{ "  " * identation }#{key}:\n#{value.to_yml(identation+1)}"
				else
					yml += "#{ "  " * identation }#{key}: \"#{value.gsub('"', '\\"')}\"\n"
				end
			end
			yml
		end

		def yml_reserved_words
			[
				"y", "Y", "yes", "Yes", "YES", "n", "N", "no",
				"No", "NO", "true", "True", "TRUE", "false", "False",
				"FALSE", "on", "On", "ON", "off", "Off", "OFF"
			]
		end

	end
end
