# RailsTranslations

A gem that syncs locales yml files and generates models attributes translation placeholders.

## Installation

Add this line to your application's Gemfile only (`recommended: for development environment only`):

```ruby
gem 'rails_translations'
```

And then execute:

    $ bundle install

## Usage

This gem can only be used from Rails console at this point. The terminal commands will be added in future releases.

## Equilizing translations files

**It's assumed that you are using default rails `config/locales/` folder organization with files which are not nested under subfolders and which have `.yml` extension.**

There are 2 commands which equilize your translation files:

### `RailsTranslations.equilize_translations(options={})`

This will go through all your translations files and merge their keys. The existing keys inside the each file will be preserved and those added from other files will have default suffix `"TODO: "`. It will not cross merge keys if you have multiple files for same locale in your `config/locales/` directory.

### `RailsTranslations.equilize_files(file_name, options={})`

This is essentially the same as the first command but only for one set of translation files. For example if you pass `"models"` as a file name it will merge the translations of *models.en.yml, models.hr.yml, models.de.yml* etc. You can list your file names with `RailsTranslations.get_translations_files_list` - this will give you the file names without `locale.yml` extension.

### Options
`equilize_locales:` accepts the array of locales you want to merge with other locales. For example, if you have `:hr, :en, :de` and you pass `RailsTranslations.equilize_translations(equilize_locales: [:hr, :de])` the missing keys in files for those locales will be added from other locales - in this case any keys from *models.hr.yml* and *models.de.yml* will not be added to *models.en.yml*.

`take_from_locales:` accepts the array of locales you want to take missing translations from. So if you pass `RailsTranslations.equilize_translations(equilize_locales: [:hr, :de])` no translations from `.en` files would be copied to the `.de` or `.hr` files.

`file_names:` accepts the array of file names you want to equilize - only for `equilize_translations` mathod. For example `RailsTranslations.equilize_translations(file_names: ["mailing", "devise"])`. Note that file name for base translation files, such as `en.yml` or `de.yml`, is empty string `""`.

`prefix:` here you specify how you want to prefix the added missing translations. Default value is `"TODO:"`. And if you pass `RailsTranslations.equilize_translations(prefix: "MISSING:")` it will add the missing translation as:
```yml
de:
  ...
  missing_translation: "MISSING: some missing translation"
  ...
```

### Usage tips

When copying missing translations the method will prioretize the locale which goes first, so if your `I18n.available_locales` are `[:en, :de, :hr]` missing translation will be copied from `:en` first. To change this order, just pass the `take_from_locales:` in prefered order.

If you pass unexisting `file_names:` the method will create set of files with that name so it can be used to generate new translation files. 

If you have `[:en, :de, :hr]` available locales and you are missing the files for one of the locales it will generate new file by mergeing other two locale files, so it can be used for generating new set of files if new available locale is added or files are simply missing.

## Building models attributes placeholders

It is used to collect models attributes and make the translations placeholders in file named `"models_fields"` by default.

`RailsTranslations.build_attributes_placeholders(options={})`

It will go through your models, collect attributes and for each your available locale generate something like:

```yml
en:
  activerecord:
    attributes:
      user:
        email: ""
        encrypted_password: ""
        reset_password_token: ""
        reset_password_sent_at: ""
        ...
```

If that file already exists, it will not override the existing translations, it will only add the missing ones.

### Options

`write_to_locales:` accepts array of locales for which you want to generate the files. `RailsTranslations.build_attributes_placeholders(write_to_locales: [:hr])` will only generate `models_fields.hr.yml`.

`file_name:` define the file name, default value is `"models_fields"`.

## Future improvements

- Use Thor to make it executable from command line.
- Pass model names to `build_attributes_placeholders` method to avoid repeteadly removing unwanted translation placeholders.
- Add module for editing translations in browser.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/vedranmijatovic0/rails_translations


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
